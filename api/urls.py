from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.api),
    url(r'^([\w]+)/lights/$', views.lights),
    url(r'^([\w]+)/lights/new/$', views.newLights),
    url(r'^([\w]+)/getlight/([1-9]+)/$', views.getLight),
    url(r'^([\w]+)/setlight/([1-9]+)/$', views.setLight),
    url(r'^([\w]+)/setlight/([1-9]+)/state/$', views.setLightState),
    url(r'^([\w]+)/deletelight/([1-9]+)/$', views.deleteLight),


]