from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from urllib import request as r
import json


def api(request):
    return HttpResponse("Hello, world. You're at the polls index.")

def lights(request, user):
    url = 'http://ip.jsontest.com/'
    response = r.urlopen(url)
    data = json.loads(response.read().decode(response.info().get_param('charset') or 'utf-8'))
    data['user'] = user
    return JsonResponse(data)

def newLights(request, user):
    url = 'http://ip.jsontest.com/'
    response = r.urlopen(url)
    data = json.loads(response.read().decode(response.info().get_param('charset') or 'utf-8'))
    return JsonResponse(data)

def getLight(request, user, light):
    url = 'http://ip.jsontest.com/'
    response = r.urlopen(url)
    data = json.loads(response.read().decode(response.info().get_param('charset') or 'utf-8'))
    return JsonResponse(data)

def setLight(request, user, light):
    url = 'http://ip.jsontest.com/'
    response = r.urlopen(url)
    data = json.loads(response.read().decode(response.info().get_param('charset') or 'utf-8'))
    return JsonResponse(data)

def setLightState(request, user, light):
    url = 'http://ip.jsontest.com/'
    response = r.urlopen(url)
    data = json.loads(response.read().decode(response.info().get_param('charset') or 'utf-8'))
    return JsonResponse(data)

def deleteLight(request, user, light):
    url = 'http://ip.jsontest.com/'
    response = r.urlopen(url)
    data = json.loads(response.read().decode(response.info().get_param('charset') or 'utf-8'))
    return JsonResponse(data)